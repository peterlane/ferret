# Ferret: Copy-Detection in Text and Code

This repository contains some implementations of the Ferret copy-detection 
tool. 

* `ferret-go` - an implementation in Go, the basis for the compiled applications provided on the webpage
* `ferret-rs` - an implementation in Rust, reproduces the Go functionality
* `ferret-scheme` - partial implementations in Scheme

Other implementations:

* [uhferret-gem](https://notabug.org/peterlane/uhferret-gem) - a Ruby wrapper based on the C++ implementation

