;; Responsible for retrieving trigrams from a given file and providing 
;; information on them.
;;
;; SchemeFerret: copy-detection program.
;; Copyright (c) 2017-22, Peter Lane.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library
  (libs trigram-reader)
  (export make-trigram-reader
          close-reader
          read-trigram
          get-last-trigram
          get-trigram-end
          get-token-start)
  (import (scheme base)
          (scheme char)
          (scheme cxr)
          (scheme file)
          (libs tokenisers))

  (cond-expand
    ((and gauche (library (file util))) 
     (import (only (file util) path-extension)))
    ((and sagittarius (library (util file)))
     (import (only (util file) path-extension)))
    (else
      (error "Your Scheme implementation has not been recognised, to access required file handling operations")))

  (begin

    (define-record-type <reader>
      (make-reader pathname port reader tokens starts ends)
      reader?
      (pathname reader-pathname)
      (port reader-port)
      (reader token-reader)
      (tokens tokens-get tokens-set!)
      (starts starts-get starts-set!)
      (ends ends-get ends-set!))

    ;; Add a new token, keep the length
    (define (push-token! reader token)
      (tokens-set! reader (list token (car (tokens-get reader)) (cadr (tokens-get reader)))))

    ;; Add a new start token, keep the length of starts
    (define (push-start! reader start)
      (starts-set! reader (list start (car (starts-get reader)) (cadr (starts-get reader)))))

    ;; Add a new end token, keep the length of ends
    (define (push-end! reader end)
      (ends-set! reader (list end (car (ends-get reader)) (cadr (ends-get reader)))))

    ;; Return a <reader> instance for given filename
    (define (make-trigram-reader filename)
      (make-reader filename (open-input-file filename)
                   (make-token-reader (path-extension filename))
                   (list #f #f #f) (list 0 0 0) (list 0 0 0)))

    ;; Close the reader's file
    (define (close-reader reader)
      (close-input-port (reader-port reader)))

    ;; Read the next trigram
    ;; Returns #t if successful, 
    ;; or #f if eof reached before a new trigram could be read
    (define (read-trigram reader)
      (parameterize ((current-input-port (reader-port reader)))
        (let loop ((next-token ((token-reader reader) (get-trigram-end reader))))
          (if (eof-object? next-token)
            #f
            (begin
              (push-token! reader (token-val next-token))
              (push-start! reader (token-start next-token))
              (push-end! reader (token-end next-token))
              (if (caddr (tokens-get reader))
                #t
                (loop ((token-reader reader) (token-end next-token)))))))))

    ;; Return the last read trigram
    (define (get-last-trigram reader)
      (let ((items (tokens-get reader)))
        (string-append (list-ref items 2) " " (list-ref items 1) " " (list-ref items 0))))

    ;; Return the document position of the end of the last trigram
    (define (get-trigram-end reader)
      (car (ends-get reader)))

    ;; Return the document position of the start of the given token 
    ;; in the last trigram
    ;; Note: tokens are 0 indexed
    (define (get-token-start reader n)
      (list-ref (starts-get reader) n))

    ))

