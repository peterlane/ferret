;; Given a file extension, creates a suitable tokeniser

;; SchemeFerret: copy-detection program.
;; Copyright (c) 2017-22, Peter Lane.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library
  (libs tokenisers)
  (export 
    make-token-reader
    token-val
    token-start
    token-end
    valid-extension?
    )
  (import (scheme base)
          (scheme char)
          (scheme hash-table)
          (scheme list))

  (begin

    (define-record-type <token> 
      (make-token token start end)
      token?
      (token token-val)
      (start token-start)
      (end token-end))

    (define *single-char-symbols*
      '("!" "%" "/" "*" "+" "-" "=" "|" "," "?" "." "&" "(" ")" "{" 
        "}" "<" ">" ":" ";" "^" "[" "]" "\"" "#" "~"))

    (define *action-script-symbols*
      '("||=" "&&=" "||" "&&" "===" "!==" ">="  "<=" "!=" "=="  
        "/*"  "*/" "//" "&="  "|="  "<<=" ">>=" "^=" "%=" ">>>" 
        ">>>=" "<<" ">>" "+=" "-=" "*="  "/=" "++" "--"))

    (define *c-code-symbols*
      '("!=" "++" "--" "==" ">=" "<=" "||" "&&" "+=" "-="
        "*=" "/=" "%=" "&=" "|=" "^=" "::" "->" "//" "<<" 
        ">>" "##" "/*" "*/" ".*" "->*" "<<=" ">>="))

    (define *c-sharp-symbols*
      '("++" "--" "->" "<<" ">>" ">=" "<=" "==" "!=" "||"
        "&&" "+=" "-=" "*=" "/=" "%=" "&=" "|=" "^=" "<<="
        ">>=" "??" "///" "/*" "*/" "//"))

    (define *groovy-symbols*
      '("!=" "++" "--" "==" ">=" "<=" "||" "&&" "+=" "-="
        "*=" "/=" "%=" "&=" "|=" "^=" "//" "<<" ">>" "##"
        "/*" "*/" "/**" "<<=" ">>=" ">>>" ">>>=" "*.@" "<=>" "=~"
        "==~" "*." ".@" "?:" "?."))

    (define *haskell-symbols*
      '("--" "{-" "-}" "^^" "**" "&&" "||" "<=" "==" "/="
        ">=" "++" ".." "::" "!!" "\\\\" "->" "<-" "=>" ">>"
        ">>=" ">@>"))

    (define *java-symbols*
      '("!=" "++" "--" "==" ">=" "<=" "||" "&&" "+=" "-="
        "*=" "/=" "%=" "&=" "|=" "^=" "//" "<<" ">>" "/*"
        "*/" "/**" "<<=" ">>=" ">>>" ">>>="))

    (define *lua-symbols*
      '("<=" ">=" "==" "~="))

    (define *php-symbols*
      '("+=" "-=" "*=" "/=" "%=" ".=" "++" "--" "!=" "=="
        "===" "<>" "!==" ">=" "<=" "||" "&&"))

    (define *prolog-symbols*
      '("=<" ">=" "==" "=:=" ":-" "?-"))

    (define *python-symbols*
      '("**" "//" ">=" "<=" "==" "!=" "<>" "!=" "+=" "-="
        "*=" "/=" "%=" "**=" "//=" "<<" ">>"))

    (define *ruby-symbols*
      '("**" ">=" "<=" "<<" ">>" "<=>" "=~" "==" "===" "!="
        "!~" "||" "&&" ".." "..." "+=" "-=" "*=" "/=" "%="
        "&=" "||=" "&&=" "<<=" ">>=" "**="))

    (define *vb-symbols*
      '(">=" "<=" "<>" "==" "+=" "-=" "*=" "/=" "\\=" "&="
        "^=" "<<" ">>"))

    (define *xml-symbols*
      '("<?" "?>" "</" "/>" "<!--" "-->"))

    (define *code-db* (list (cons "as" *action-script-symbols*)
                            (cons "actionscript" *action-script-symbols*)
                            (cons "c" *c-code-symbols*)
                            (cons "h" *c-code-symbols*)
                            (cons "cpp" *c-code-symbols*)
                            (cons "cs" *c-sharp-symbols*)
                            (cons "groovy" *groovy-symbols*)
                            (cons "hs" *haskell-symbols*)
                            (cons "lhs" *haskell-symbols*)
                            (cons "java" *java-symbols*)
                            (cons "lua" *lua-symbols*)
                            (cons "php" *php-symbols*)
                            (cons "pl" *prolog-symbols*)
                            (cons "py" *python-symbols*)
                            (cons "rb" *ruby-symbols*)
                            (cons "vb" *vb-symbols*)
                            (cons "html" *xml-symbols*)
                            (cons "xml" *xml-symbols*)))
    (define *lisp-extensions* '("clj" "lisp" "lsp" "rkt" "scm" "ss" "sld" "sls" "sps"))
    (define *text-extensions* '("txt"))

    ;; Checks if given extension is understood by Ferret
    (define (valid-extension? extn)
      (and (string? extn)
           (member extn 
                   (append (map car *code-db*)
                           *lisp-extensions*
                           *text-extensions*))))

    ;; Returns a read-token function specialised to the given extension
    ;; Reports an error if the extension is unknown
    (define (make-token-reader extn)
      (cond ((assoc extn *code-db*)
             (read-code-token (cdr (assoc extn *code-db*))))
            ((member extn *lisp-extensions*)
             read-lisp-token)
            ((member extn *text-extensions*)
             read-text-token)
            (else
              (error "Unknown extension for make-token-reader " extn))))

    ;; Skip blanks, returning a count of number skipped
    (define (skip-blanks)
      (let ((blanks-read 0))
        ; skip any white space
        (do ((c (peek-char) (peek-char)))
          ((or (eof-object? c)
               (not (char-whitespace? c))) 
           blanks-read) ; return number of blanks at end
          (read-char)
          (set! blanks-read (+ 1 blanks-read)))))

    ;; Returns a function to read token for code specialised to given symbol-list
    ;; -- this matches many programming languages, with the main variation being the symbols
    (define (read-code-token symbol-list)
      (let ((symbols (make-hash-table eqv?)))
        (for-each (lambda (sym) (hash-table-set! symbols (reverse (string->list sym)) 1))
                  (append *single-char-symbols* symbol-list))
        ;
        (lambda (start-posn) 
          ;
          (define (is-number? char previous-chars)
            (or (char-numeric? char)
                (char=? char #\.)))               ; no test for single . in number
          ;
          (define (is-symbol? char previous-chars)
            (hash-table-contains? symbols (cons char previous-chars)))
          ;
          (define (is-name? char previous-chars)
            (or (char-alphabetic? char)
                (char-numeric? char)
                (char=? char #\_)))
          ;
          (define (read-while pred? start-char)
            (do ((res (list start-char) (cons (read-char) res)))
              ((or (eof-object? (peek-char))
                   (not (pred? (peek-char) res)))
               (list->string (reverse res)))))
          ;
          (let ((blanks-read (skip-blanks)))
            ; check type of next char, to determine type of token to read
            (let ((c (read-char)))
              (if (eof-object? c) ; hit end of file, no token read
                  c
                  (let* ((predicate (cond ((is-number? c '()) is-number?)
                                          ((is-symbol? c '()) is-symbol?)
                                          (else is-name?)))
                         (tok (read-while predicate c)))
                    (make-token tok
                                (+ start-posn blanks-read)
                                (+ start-posn blanks-read (string-length tok))))))))))

    (define (read-lisp-token start-posn)
      (let ((blanks-read (skip-blanks)))
        ; check type of next char, to determine type of token to read
        (let ((c (read-char)))
          (cond ((eof-object? c) ; hit end of file, no token read
                 c)
                ((or (char=? c #\() (char=? c #\))) ; found a bracket, that is a token
                 (make-token (string c)
                             (+ start-posn blanks-read)
                             (+ start-posn blanks-read 1)))
                (else ; must be a symbol - read until space or ( or )
                  (do ((res (list c) (cons (read-char) res))
                       (nxt (peek-char) (peek-char)))
                    ((or (eof-object? nxt)
                         (char-whitespace? nxt)
                         (char=? nxt #\()
                         (char=? nxt #\)))
                     (make-token (list->string (reverse res))
                                 (+ start-posn blanks-read)
                                 (+ start-posn blanks-read (length res))))))))))

    (define (read-text-token start-posn)
      (let ((ignored-chars 0))
        ; skip any non-alphabetic characters
        (do ((c (peek-char) (peek-char)))
          ((or (eof-object? c)
               (char-alphabetic? c)) )
          (read-char)
          (set! ignored-chars (+ 1 ignored-chars)))
        ; check type of next char, unless at end of file
        (let ((c (read-char)))
          (cond ((eof-object? c) ; hit end of file, no token read
                 c)
                (else ; must be a symbol - read until space or ( or )
                  (do ((res (list c) ;(cons (read-char) res))
                            (let ((nc (read-char)))
                              (if (eof-object? nc) ; ?? Chez needs extra test on files which end on a char
                                  res
                                  (cons nc res))))
                       (nxt (peek-char) (peek-char)))
                    ((or (eof-object? nxt)
                         (not (char-alphabetic? nxt)))
                     (make-token (list->string (reverse (map char-downcase res)))
                                 (+ start-posn ignored-chars)
                                 (+ start-posn ignored-chars (length res))))))))))

    ))

