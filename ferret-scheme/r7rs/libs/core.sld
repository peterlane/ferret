;; SchemeFerret: copy-detection program.
;; Copyright (c) 2017-22, Peter Lane.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library
  (libs core)
  (export run-ferret
          file-filename
          file-pathname
          file?
          files->compared-pairs
          write-comparison-xml
          )
  (import (scheme base)
          (scheme cxr)
          (scheme comparator)
          (scheme hash-table)
          (scheme set)
          (scheme sort)
          (libs tokenisers)
          (libs trigram-reader))

  (cond-expand
    ((and gauche (library (file util))) 
     (import (only (file util) decompose-path directory-fold path-extension))
     (begin
       (define (get-file-list directory)
         (directory-fold directory cons '()))
       (define (path-basename path) 
         (let-values (((basepath basename extn) (decompose-path path)))
           basename))
       ))
    ((and sagittarius (library (util file)))
     (import (only (util file) find-files path-basename path-extension))
     (begin
       (define (get-file-list directory)
         (find-files directory))
       ))
    (else
      (error "Your Scheme implementation has not been recognised, to access required file handling operations")))

  (begin

    ;; Record to hold all information for a single pass of ferret
    (define-record-type <documents>
      (new-documents files tmap matches)
      documents?
      (files get-files set-files!)
      (tmap get-tmap)
      (matches get-matches))

    (define (make-documents)
      (new-documents '() (make-hash-table string=?) (make-hash-table equal?)))

    ;; key for matches is a list of ids in numeric order
    (define (matches-key file1 file2)
      (let ((file-id1 (if (file? file1) (file-id file1) file1))
            (file-id2 (if (file? file2) (file-id file2) file2)))
        (if (< file-id1 file-id2)
            (list file-id1 file-id2)
            (list file-id2 file-id1))))

    ;; operations

    (define (add-file documents file)
      (set-files! documents (cons file (get-files documents))))

    (define (add-trigram documents file trigram)
      (let ((trigram-map (get-tmap documents)))
        (unless (hash-table-contains? trigram-map trigram)
          (hash-table-set! trigram-map trigram (set (make-comparator integer? = < number-hash))))
        (let ((file-set (hash-table-ref/default trigram-map trigram '())))
          (unless (set-contains? file-set (file-id file)) ; add 1 for *new* trigrams only
            (file-trigram-count-set! file (+ 1 (file-trigram-count file)))
            (set-adjoin! file-set (file-id file))))))

    (define (compute-matches documents)
      (let* ((keys (hash-table-keys (get-tmap documents)))
             (size (length keys)))
        (do ((i 0 (+ i 1)))
          ((= i size) )
          (let* ((mf (list->vector (set->list (hash-table-ref/default (get-tmap documents) (list-ref keys i) '()))))
                 (mf-length (vector-length mf))
                 (matches (get-matches documents)))
            (let loop ((i 0)
                       (j 1))
              (cond ((>= i mf-length) ) ; done
                    ((>= j mf-length) (loop (+ i 1) (+ i 2))) ; next row
                    (else (hash-table-update!/default matches ; add one to match for (i j) pairing
                                                      (matches-key (vector-ref mf i)
                                                                   (vector-ref mf j))
                                                      (lambda (n) (+ 1 n))
                                                      0)
                          (loop i (+ 1 j)))))))))

    (define (contains-trigram? documents file trigram)
      (let ((file-set (hash-table-ref/default (get-tmap documents) trigram #f)))
        (and (set? file-set)
             (set-contains? file-set (file-id file)))))

    ;; Computes Jaccard correlation of trigrams in the two files
    (define (compute-resemblance documents file1 file2)
      (let* ((num-matches (hash-table-ref (get-matches documents)
                                         (matches-key file1 file2)
                                         0))
             (total-trigrams (+ (file-trigram-count file1) 
                                (file-trigram-count file2)
                                (- num-matches))))
        (if (zero? total-trigrams)
            0
            (/ num-matches total-trigrams))))

    ;; Computes proportion of common trigrams with respect to second file
    (define (compute-containment documents file1 file2)
      (let* ((num-matches (hash-table-ref (get-matches documents)
                                         (matches-key file1 file2)
                                         0))
             (target-trigrams (file-trigram-count file2)))
        (if (zero? target-trigrams)
            0
            (/ num-matches target-trigrams))))


    (define-record-type <file>
      (new-file id pathname filename trigram-count)
      file?
      (id file-id)
      (pathname file-pathname)
      (filename file-filename)
      (trigram-count file-trigram-count file-trigram-count-set!))

    (define make-file 
      (let ((next-id -1))
        (lambda (pathname filename) 
          (set! next-id (+ 1 next-id))
          (new-file next-id pathname filename 0))))

    ;; Collect files from directory into given documents
    (define (collect-files directory documents)
      (for-each (lambda (filename)
                  (when (valid-extension? (path-extension filename))
                    (add-file documents
                              (make-file filename (path-basename filename)))))
                (get-file-list directory)))

    ;; Read the trigrams in given file, storing data in documents 
    (define (read-trigrams documents file)
      (let ((reader (make-trigram-reader (file-pathname file))))
        (let loop ()
          (cond ((read-trigram reader)
                 (add-trigram documents file (get-last-trigram reader))
                 (loop))
                (else
                  (close-reader reader)))))
      file)

    ;; Given a directory name, run Ferret on the files within it and compute statistics
    (define (run-ferret dirname)
      (let ((documents (make-documents)))
        (collect-files dirname documents)
        (for-each 
          (lambda (file) (read-trigrams documents file))
          (get-files documents))
        (compute-matches documents)
        documents))

    ;; Given some files, return a list of all pairs of the files, with their resemblance
    ;; sort by the resemblance score
    (define (files->compared-pairs documents grouped?)
      (define (in-same-group? file1 file2)
        (and grouped?
             (string=? (path-first (file-filename file1))
                       (path-first (file-filename file2)))))
      ;
      (let ((lst (list->vector (get-files documents))))
        (let loop ((i 0)
                   (j 1)
                   (res '()))
          (cond ((= i (vector-length lst))
                 (list-sort (lambda (a b) (> (caddr a) (caddr b)))
                            res) )
                ((= j (vector-length lst))
                 (loop (+ 1 i)
                       (+ 2 i)
                       res))
                (else
                  (let ((file1 (vector-ref lst i))
                        (file2 (vector-ref lst j)))
                    (loop i 
                          (+ 1 j)
                          (if (in-same-group? file1 file2)
                              res
                              (cons (list file1 file2 (inexact (compute-resemblance documents file1 file2))) 
                                    res)))))))))

    ;; Writes the comparison of two files in xml format to current output port
    ;; outfile is name of output file, or #f for output to stdout
    (define (write-comparison-xml filename1 filename2 outfile)
      (define (write-header documents file1 file2)
        (format #t 
                (string-append
                  "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
                  "<?xml-stylesheet type=\"text/xsl\" href=\"uhferret2.xsl\" ?>"
                  "<uhferret>"

                  "<common-trigrams>~d</common-trigrams>"
                  "<resemblance>~5,3f</resemblance>")
                (hash-table-ref (get-matches documents) (matches-key file1 file2) 0)
                (inexact (compute-resemblance documents file1 file2))))
      ;
      (define (write-doc documents file1 file2) ; writes file1 compared to file2
        (define (write-doc-head)
          (format #t
                  (string-append "<document>"
                                 "<source>~a</source>"
                                 "<num-trigrams>~d</num-trigrams>"
                                 "<containment>~5,3f</containment>"
                                 "<text>")
                  (file-pathname file1)
                  (file-trigram-count file1)
                  (inexact (compute-containment documents file1 file2))))
        ;
        (define (write-doc-text) 
          (let ((source-text 
                  (with-input-from-file (file-pathname file1)
                                        (lambda () 
                                          (do ((line (get-line (current-input-port)) (get-line (current-input-port)))
                                               (txt '() (cons line txt)))
                                            ((eof-object? line) 
                                             (fold-left (lambda (r s) (string-append s "\n" r)) "" txt))))))
                (last-written 0)
                (inside-block? #f)
                (reader (make-trigram-reader (file-pathname file1))))
            ;; loop through text
            (do ((got-one? (read-trigram reader) (read-trigram reader)))
              ((not got-one?) ) ; finished
              (cond ((contains-trigram? documents file2 (get-last-trigram reader))
                     (unless inside-block?
                       (when (> last-written 0)
                         (format #t "]]></block>")) ; end the last block
                       (format #t "<block text=\"same\"><![CDATA[") ; start same block
                       (set! inside-block? #t))
                     (format #t "~a" (substring source-text 
                                                last-written
                                                (get-trigram-end reader)))
                     (set! last-written (get-trigram-end reader)))
                    (else ; not a shared trigram
                      (when (< last-written (get-token-start reader 1))
                        (when (or inside-block? ; check if moving from inside same block to unique
                                  (zero? last-written))
                          (when (> last-written 0)
                            (format #t "]]></block>")) ; end the last block
                          (format #t "<block text=\"unique\"><![CDATA[") ; start unique block
                          (set! inside-block? #f))
                        (format #t "~a" (substring source-text
                                                   last-written
                                                   (get-token-start reader 1)))
                        (set! last-written (get-token-start reader 1))))))
            ;; tidy up by writing any remaining text
            (when (< last-written (string-length source-text))
              (when inside-block?
                (format #t "]]></block>") ; end the last block
                (set! inside-block? #f)
                (format #t "<block text=\"unique\"><![CDATA[")) ; start unique block for remainder
              (format #t "~a" (substring source-text last-written (string-length source-text)))) ; finish whole of source
            (unless (zero? last-written) ; nothing has been written
              (format #t "]]></block>")))) ; end the last block
        ;
        (define (write-doc-tail)
          (format #t "</text></document>"))
        ;
        (write-doc-head)
        (write-doc-text)
        (write-doc-tail))
      ;
      (define (write-trailer)
        (format #t "</uhferret>"))
      ;
      (define (write-xml-report documents file1 file2)
        (write-header documents file1 file2)
        (write-doc documents file1 file2)
        (write-doc documents file2 file1)
        (write-trailer))
      ;
      (let* ((documents (make-documents))
             (file1 (read-trigrams documents (make-file filename1 filename1)))
             (file2 (read-trigrams documents (make-file filename2 filename2))))
        (if outfile ; if output filename given, output to that file, else to stdout
            (parameterize ((current-output-port (open-output-file outfile)))
              (write-xml-report documents file1 file2))
            (write-xml-report documents file1 file2))))

    ))

