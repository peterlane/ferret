;; Scheme version of UH-Ferret for R7RS Scheme

;; SchemeFerret: copy-detection program.
;; Copyright (c) 2017-23, Peter Lane.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(import (scheme base)
        (scheme case-lambda)
        (scheme cxr)
        (scheme process-context)
        (scheme write)
        (libs core))

;; Display paired results on screen
(define display-results
  (case-lambda
    ((files)
     (display-results files #f))
    ((files grouped?)
     (newline)
     (display "---- Similarity Scores ----")
     (newline)
     (for-each (lambda (res)
                 (display (file-pathname (car res))) (display " ") (display (file-pathname (cadr res)))
                 (display " ") (display (caddr res)) (newline))
               (files->compared-pairs files grouped?)))))

;; The main ferret program
(define (ferret)
  (let ((args (command-line)))
    (cond 
      ((= 2 (length args))
       (display-results (run-ferret (cadr args))))
      ((and (= 3 (length args))
            (string-ci=? "-g" (cadr args)))
       (display-results (run-ferret (caddr args)) #t))
      ((and (memq (length args) '(4 5))
            (string-ci=? "-x" (cadr args)))
       (let ((outfile (and (= 5 (length args)) (list-ref args 4))))
         (write-comparison-xml (caddr args) (cadddr args) outfile)))
      (else
        (display "Usage: schemeferret") (newline)
        (display "          dirname                       to compare all files in given directory") (newline)
        (display "       -g dirname                       use subdirectory names to group files") (newline)
        (display "       -x filename1 filename2 [outfile] to output an XML comparison of given filenames") (newline)
        ))))

(ferret)

