# Ferret

Ferret is a copy-detection tool, locating duplicate text or code in 
multiple text documents or source files.  Ferret is designed to  
detect copying ( _collusion_ ) within a given set of files.

This version is a Scheme implementation of Ferret. Two versions are given: for
practical use, the Chez Scheme version is preferable because of its execution
speed.

For more about Ferret see http://peterlane.codeberg.page/ferret/

## Chez Scheme Implementation

This version is designed for Chez Scheme.

```
> scheme.exe --libdirs . --program .\chezferret.ss
Usage: chezferret
          dirname                       to compare all files in given directory
       -g dirname                       use subdirectory names to group files
       -x filename1 filename2 [outfile] to output an XML comparison of given filenames
```

## R7RS Implementation

This version requires an R7RS implementation of Scheme such as Gauche.

Run as:

```
> gosh -I. schemeferret.sps
Usage: schemeferret
          dirname                       to compare all files in given directory
       -g dirname                       use subdirectory names to group files
       -x filename1 filename2 [outfile] to output an XML comparison of given filenames
```

