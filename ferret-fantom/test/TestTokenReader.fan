class TestTokenReader : Test
{
  Void testValidExtension()
  {
    verifyTrue(TokenReader.validExtension("java"))
    verifyTrue(TokenReader.validExtension("ss"))
    verifyTrue(TokenReader.validExtension("txt"))
    verifyFalse(TokenReader.validExtension("txts"))
  }

  Void testMakeFor()
  {
    verifyType(TokenReader.makeFor("java", Env.cur.in), CodeReader#)
    verifyType(TokenReader.makeFor("ss", Env.cur.in), LispReader#)
    verifyType(TokenReader.makeFor("txt", Env.cur.in), TextReader#)
    verifyErr(Err#) { TokenReader.makeFor("txts", Env.cur.in) }
  }

  Void testSkipBlanks()
  {
    [
      ["abc", ""],
      ["  abc", "  "],
      ["  ", "  "],
      [" \t\na", " \t\n"]
    ].each |Str[] testCase|
    {
      reader := TokenReader.makeFor("txt", testCase[0].in)
      verifyEq(testCase[1], reader.skipBlanks)
    }
  }

  Void testJavaReader()
  {
    doTestReader(
      "java",
      [
        TestCase("int x+=3;", [
          ReaderResult("", "int"),
          ReaderResult(" ", "x"),
          ReaderResult("", "+="),
          ReaderResult("", "3"),
          ReaderResult("", ";")
        ])
      ])
  }

  Void testLispReader()
  {
    doTestReader(
      "lisp",
      [
        TestCase("(define)", [
          ReaderResult("", "("), 
          ReaderResult("", "define"), 
          ReaderResult("", ")")
        ]),
        TestCase("(define )", [
          ReaderResult("", "("), 
          ReaderResult("", "define"), 
          ReaderResult(" ", ")")
        ])
      ])
  }

  Void testTextReader()
  {
    doTestReader(
      "txt",
      [
        TestCase("abc", [
          ReaderResult("", "abc")
        ]),
        TestCase("  abc2def", [
          ReaderResult("  ", "abc"),
          ReaderResult("2", "def")
        ]),
        TestCase("abc. DE3fg", [
          ReaderResult("", "abc"),
          ReaderResult(". ", "de"),
          ReaderResult("3", "fg")
        ])
      ])
  }

  Void doTestReader(Str extension, TestCase[] testCases)
  {
    testCases.each |TestCase testCase|
    {
      reader := TokenReader.makeFor(extension, testCase.text.in)
      results := ReaderResult[,]
      for (;;)
      {
        result := reader.readToken()
        if (result.error)
          break
        else
          results.add(result)
      }
      verifyEq(results.size, testCase.results.size)
      results.each |ReaderResult result, Int index|
      {
        verifyEq(result.prestring, testCase.results[index].prestring)
        verifyEq(result.token, testCase.results[index].token)
      }
    }
  }
}

class TestCase
{
  Str text
  ReaderResult[] results

  new make(Str text, ReaderResult[] results)
  {
    this.text = text
    this.results = results
  }
}

