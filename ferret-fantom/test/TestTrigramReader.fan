class TestTrigramReader : Test
{
  Void testJavaTrigramReader()
  {
    reader := TrigramReader("int x+=3;".in, "java")
    verifyTrue(reader.readTrigram)
    verifyEq("int x +=", reader.lastTrigram)
    verifyTrue(reader.readTrigram)
    verifyEq("x += 3", reader.lastTrigram)
    verifyTrue(reader.readTrigram)
    verifyEq("+= 3 ;", reader.lastTrigram)
    verifyFalse(reader.readTrigram)
  }

  Void testLispTrigramReader()
  {
    reader := TrigramReader("(abc b)".in, "lisp")
    verifyTrue(reader.readTrigram)
    verifyEq("( abc b", reader.lastTrigram)
    verifyTrue(reader.readTrigram)
    verifyEq("abc b )", reader.lastTrigram)
    verifyFalse(reader.readTrigram)
  }

  Void testTextTrigramReaderEmptyStr()
  {
    reader := TrigramReader("".in, "txt")
    verifyFalse(reader.readTrigram)
  }

  Void testTextTrigramReaderTwoWords()
  {
    reader := TrigramReader("abc def".in, "txt")
    verifyFalse(reader.readTrigram)
  }

  Void testTextTrigramReader()
  {
    reader := TrigramReader("some. wORds, with, punctuation 123 Numbers".in, "txt")
    verifyTrue(reader.readTrigram)
    verifyEq("some words with", reader.lastTrigram)
    verifyTrue(reader.readTrigram)
    verifyEq("words with punctuation", reader.lastTrigram)
    verifyTrue(reader.readTrigram)
    verifyEq("with punctuation numbers", reader.lastTrigram)
    verifyFalse(reader.readTrigram)
  }
}
