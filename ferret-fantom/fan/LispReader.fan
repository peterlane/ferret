class LispReader : TokenReader
{
  new make(InStream in) : super.make(in)
  {
  }

  override ReaderResult readToken()
  {
    prestring := skipBlanks()
    if (in.peekChar == null) 
    {
      return ReaderResult.makeError(prestring)
    }
    else if (['(', ')'].contains(in.peekChar))
    {
      return ReaderResult(prestring, in.readChar.toChar)
    }
    else // must be a symbol - read until EOF, space or ( )
    {
      result := StrBuf()
      while (
        in.peekChar != null && 
        !in.peekChar.isSpace && 
        !['(', ')'].contains(in.peekChar)
      )
      {
        result.addChar(in.readChar)
      }

      return ReaderResult(prestring, result.toStr)
    }
  }
}
