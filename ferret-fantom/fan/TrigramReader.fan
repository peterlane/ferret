class TrigramReader
{
  TokenReader reader
  ReaderResult[] tokens

  new make(InStream in, Str extension)
  {
    reader = TokenReader.makeFor(extension, in)
    tokens = ReaderResult[,]
  }

  ** Reads the next trigram, returning true if successful.
  ** Returns false if EOF before trigram could be read.
  Bool readTrigram()
  {
    token := reader.readToken()
    if (!token.error) 
    {
      tokens.add(token)
    }

    while (!token.error && tokens.size < 3)
    {
      token = reader.readToken()
      if (!token.error) 
      {
        tokens.add(token)
      }
    }
    if (tokens.size > 3) 
    {
      tokens = tokens[1..3]
    }

    return !token.error
  }

  ** Return string representation of last trigram read.
  Str lastTrigram()
  {
    return tokens.map { it.token }.join(" ")
  }
}
