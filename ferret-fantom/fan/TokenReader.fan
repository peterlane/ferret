abstract class TokenReader
{
  InStream in { private set }

  new make(InStream in)
  {
    this.in = in
  }

  ** Create an appropriate reader type for the given extension.
  ** Raises an Err if the extension is not recognised.
  static TokenReader makeFor(Str extension, InStream in)
  {
    codeDefn := codeDefinitions.find { it.extensions.contains(extension) }
    if (codeDefn != null)
    {
      return CodeReader(
        in, 
        singleCharDefinitions.union(codeDefn.symbols)
      )
    }
    else if (lispExtensions.contains(extension))
    {
      return LispReader(in)
    }
    else if (textExtensions.contains(extension))
    {
      return TextReader(in)
    }
    else
    {
      throw Err("No reader available for $extension")
    }
  }

  ** Check if given extension is recognised.
  static Bool validExtension(Str extension)
  {
    return codeDefinitions.any { it.extensions.contains(extension) } ||
      lispExtensions.contains(extension) ||
      textExtensions.contains(extension)
  }

  ** Moves past any whitespace characters, returning string with blanks
  Str skipBlanks()
  {
    blanks := StrBuf()

    while (in.peekChar != null && in.peekChar.isSpace) 
    {
      blanks.addChar(in.readChar)
    }

    return blanks.toStr
  }

  ** Moves past non alphabetic characters, returning string with the 
  ** nonalphabetic chars.
  Str skipNonAlphabetics()
  {
    nonAlphabetics := StrBuf()

    while (in.peekChar != null && !in.peekChar.isAlpha)
    {
      nonAlphabetics.addChar(in.readChar)
    }

    return nonAlphabetics.toStr
  }

  ** Override in child classes
  abstract ReaderResult readToken()

  ** Code extension:symbol-lisp map
  const static Str[] singleCharDefinitions := [
    "!", "%", "/", "*", "+", "-", "=", "|", ",",
		"?", ".", "&", "(", ")", "{", "}", "<", ">", ":", ";", "^", "[", "]",
		"\"", "#", "~"
  ]
  const static CodeDefinition[] codeDefinitions := [
    CodeDefinition(["as", "actionscript"], [
      "||=", "&&=", "||", "&&", "===", "!==", ">=",
		  "<=", "!=", "==", "/*", "*/", "//", "&=", "|=", "<<=", ">>=", "^=", "%=",
		  ">>>", ">>>=", "<<", ">>", "+=", "-=", "*=", "/=", "++", "--"
    ]),
    CodeDefinition(["c", "h", "cpp"], [
      "!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
	  	"*=", "/=", "%=", "&=", "|=", "^=", "::", "->", "//", "<<",
  		">>", "##", "/*", "*/", ".*", "->*", "<<=", ">>="
    ]),
    CodeDefinition(["cs"], [
      "++", "--", "->", "<<", ">>", ">=", "<=", "==", "!=", "||",
	  	"&&", "+=", "-=", "*=", "/=", "%=", "&=", "|=", "^=", "<<=",
  		">>=", "??", "///", "/*", "*/", "//"
    ]),
    CodeDefinition(["go"], [
      "+=", "&=", "&&", "==", "!=", "-=", "|=", "||",
	  	"*=", "^=", "<-", ">=", "<<", "/=", "<<=", "++", ":=", ">>", "%=",
  		">>=", "--", "...", "&^", "&^=", "//", "/*", "*/"
    ]),
    CodeDefinition(["groovy"], [
      "!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
		  "*=", "/=", "%=", "&=", "|=", "^=", "//", "<<", ">>", "##",
	  	"/*", "*/", "/**", "<<=", ">>=", ">>>", ">>>=", "*.@", "<=>", "=~",
  		"==~", "*.", ".@", "?:", "?."
    ]),
    CodeDefinition(["hs", "lhs"], [
      "--", "{-", "-}", "^^", "**", "&&", "||", "<=", "==", "/=",
	  	">=", "++", "..", "::", "!!", "\\\\", "->", "<-", "=>", ">>",
  		">>=", ">@>"
    ]),
    CodeDefinition(["java"], [
      "!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
	  	"*=", "/=", "%=", "&=", "|=", "^=", "//", "<<", ">>", "/*",
  		"*/", "/**", "<<=", ">>=", ">>>", ">>>="
    ]),
    CodeDefinition(["lua"], [
      "<=", ">=", "==", "~="
    ]),
    CodeDefinition(["php"], [
      "+=", "-=", "*=", "/=", "%=", ".=", "++", "--", "!=", "==",
		  "===", "<>", "!==", ">=", "<=", "||", "&&"
    ]),
    CodeDefinition(["py"], [
      "**", "//", ">=", "<=", "==", "!=", "<>", "!=", "+=", "-=",
  		"*=", "/=", "%=", "**=", "//=", "<<", ">>"
    ]),
    CodeDefinition(["rb"], [
      "**", ">=", "<=", "<<", ">>", "<=>", "=~", "==", "===", "!=",
		  "!~", "||", "&&", "..", "...", "+=", "-=", "*=", "/=", "%=",
		  "&=", "||=", "&&=", "<<=", ">>=", "**="
    ]),
    CodeDefinition(["rs"], [
      "!=", "%=", "&=", "&&", "*=", "+=", "-=", "->", "..", "..=", "...",
       "/=", "<<", "<<=", "<=", "==", "=>", ">=", ">>", ">>=", "^=", "|=",
       "||", "::", "//", "//!", "///", "/*", "*/", "/*!", "/**"
     ]),
    CodeDefinition(["vb"], [
      ">=", "<=", "<>", "==", "+=", "-=", "*=", "/=", "\\=", "&=",
		  "^=", "<<", ">>"
    ]),
    CodeDefinition(["html", "xml"], [
      "<?", "?>", "</", "/>", "<!--", "-->"
    ])
  ]
  ** Acceptable lisp extensions
  const static Str[] lispExtensions := [
    "clj", "lisp", "lsp", "rkt", "scm", "ss", "sld", "sls", "sps"
  ]
  ** Acceptable text extensions
  const static Str[] textExtensions := [
    "adoc", "md", "txt"
  ]
}

const class CodeDefinition
{
  const Str[] extensions
  const Str[] symbols

  new make(Str[] extensions, Str[] symbols)
  {
    this.extensions = extensions
    this.symbols = symbols
  }
}
