const class ReaderResult
{
  const Str prestring // any blanks preceding the token
  const Str token     // token text
  const Bool error    // flag if result failed to read a token

  new make(Str prestring, Str token)
  {
    this.prestring = prestring
    this.token = token
    this.error = false
  }

  new makeError(Str prestring)
  {
    this.prestring = prestring
    this.token = ""
    this.error = true
  }
}
