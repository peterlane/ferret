class CodeReader : TokenReader
{
  Str[] symbols

  new make(InStream in, Str[] symbols) : super.make(in)
  {
    this.symbols = symbols
  }

  ** For code, there are three broad categories: numbers, symbols, identifiers
  ** -- numbers are assumed to be made from numbers and . (is code localised?)
  ** -- symbols are contained in the symbols set, based on the file extension
  ** -- identifiers are the rest
  ** This produces a useful set of tokens, especially for syntactically correct 
  ** input code.
  ** Comments are processed, but also treated as code.
  override ReaderResult readToken()
  {
    // function to test if character is a number
    isNumber := |Int c -> Bool| { return c.isDigit || c == '.' }
    // function to test if character is part of symbol
    isSymbol := |Str s, Int c -> Bool| { return symbols.contains(s + c.toChar) }
    // function to test if character is part of identifier
    isIdentifier := |Int c -> Bool| { return c.isAlphaNum || c == '_' }

    prestring := skipBlanks()
    if (in.peekChar == null)
    {
      return ReaderResult.makeError(prestring)
    }
    else
    {
      result := StrBuf()

      if (isNumber(in.peekChar))
      {
        while (in.peekChar != null && isNumber(in.peekChar))
        {
          result.addChar(in.readChar)
        }
      }
      else if (isSymbol(result.toStr, in.peekChar))
      {
        while (in.peekChar != null && isSymbol(result.toStr, in.peekChar))
        {
          result.addChar(in.readChar)
        }
      }
      else // identifier
      {
        while (in.peekChar != null && isIdentifier(in.peekChar))
        {
          result.addChar(in.readChar)
        }
      }

      return ReaderResult(prestring, result.toStr)
    }
  }
}
