class TextReader : TokenReader
{
  new make(InStream in) : super.make(in)
  {
  }

  override ReaderResult readToken()
  {
    prestring := skipNonAlphabetics()
    if (in.peekChar == null)
    {
      return ReaderResult.makeError(prestring)
    }
    else
    {
      result := StrBuf()
      while (in.peekChar != null && in.peekChar.isAlpha)
      {
        result.addChar(in.readChar.lower)
      }

      return ReaderResult(prestring, result.toStr)
    }
  }
}
