using build

class Build : build::BuildPod
{
  new make()
  {
    podName = "uhferret"
    summary = "A trigram-based tool for detecting similarity in groups of text documents or program code."
    version = Version("1.0")
    depends = ["sys 1.0"]
    srcDirs = [`fan/`, `test/`]
  }
}
