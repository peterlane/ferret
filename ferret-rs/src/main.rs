use std::env;
use std::process;

use ferret::documents::*;

const VERSION : &'static str = "1.0.0";

fn main() {
    let mut args: Vec<String> = env::args().collect ();
    args.remove (0); // remove the name of the program from list of args

    match args.len () {
        0 => error_message ("No filenames provided"),
        1 => {
            match args[0].as_str () {
                "-h" | "--help" => help_message (),
                "-v" | "--version" => version_message (),
                _ => (),
            }
        },
        4 => {
            let arg = args[0].as_str ();
            if arg == "-x" || arg == "--xml-report" {
                xml_report (&args[1], &args[2], &args[3]);
            }
        },
        _ => (),
    }

    let mut do_group = false;
    let mut do_list = false;
    let mut do_unique = false;
    loop {
        if args.len () > 0 {
            let arg = args[0].as_str ();
            if arg == "-g" || arg == "--group" {
                do_group = true;
                args.remove (0);
                continue;
            }
            if arg == "-l" || "arg" == "--list-trigrams" {
                do_list = true;
                args.remove (0);
                continue;
            }
            if arg == "-u" || "arg" == "--unique-counts" {
                do_unique = true;
                args.remove (0);
                continue;
            }
        }
        break;
    }

    run_ferret (&args, do_group, do_list, do_unique);
}

fn error_message (error: &str) {
    println!("Error: {}", error);
    help_message ();
}

fn help_message () {
    println!("Usage: ferret [-ghluvx] filename [filenames...]
 -g, --group       Use subdirectory names to group files
 -h, --help        Show help information
 -l, --list-trigrams
                   Output list of trigrams found
 -u, --unique-counts
                   Output counts of unique trigrams
 -v, --version     Version number
 -x, --xml-report  filename1 filename2 outfile : Create XML report");
    process::exit (0);
}

// Output similarity results in a CSV format to stdout
fn output_results (docs : &Documents, do_group : bool) {
    for result in docs.sorted_results(do_group).iter () {
        println!("{}", result);
    }
}

// Output list of trigrams in a CSV format to stdout
fn output_trigrams (docs : &Documents) {
    for result in docs.trigram_list().iter () {
        println!("{}", result);
    }
}

// Output filename - unique counts in a CSV format to stdout
fn output_unique_counts (docs : &Documents, do_group : bool) {
    for result in docs.sorted_unique_counts(do_group).iter () {
        println!("{}", result);
    }
}

// Run ferret on all files/directories in 'files', and then output based on 
// given settings
fn run_ferret (files: &[String], do_group: bool, do_list: bool, do_unique: bool) {
    let docs = Documents::new (&files);

    if do_list {
        output_trigrams (&docs);
    } else if do_unique {
        output_unique_counts (&docs, do_group);
    } else {
        output_results (&docs, do_group);
    }
}

fn version_message () {
    println!("ferret version {}", VERSION);
    process::exit (0);
}

// Run ferret on the two given files, then output an xml report to the provided
// output file.
fn xml_report (filename1: &String, filename2: &String, outfile: &String) {
    let files = [filename1.clone (), filename2.clone ()];
    let docs = Documents::new (&files);

    if let Ok(mut file) = std::fs::File::create(outfile) {
        docs.write_xml (&docs.files[0], &docs.files[1], &mut file).unwrap ();
    }

    process::exit (0);
}
