//! Ferret is a copy-detection tool, locating duplicate text or code in multiple text documents or
//! source files. Ferret is designed to detect copying (_collusion_) within a given set of files.
//!
//! As a library, Ferret can be used to analyse program code or natural language texts into 
//! trigrams, and compare pairs of documents for similarity.

pub mod chardrip;
pub mod documents;
pub mod tokenisers;
pub mod trigram_reader;
