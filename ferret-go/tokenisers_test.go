/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"io"
	"strings"
	"testing"
)

type testcase struct {
	str  string
	toks [][]string
}

func TestSkipBlanks(t *testing.T) {
	testcases := []struct {
		str string
		pre string
	}{
		{"abc", ""},
		{"  abc", "  "},
		{"  ", "  "},
		{" \t\na", " \t\n"},
	}

	for _, test := range testcases {
		reader := TokenData{bufio.NewReader(strings.NewReader(test.str))}
		actual := reader.SkipBlanks()
		if test.pre != actual {
			t.Errorf("SkipBlanks on |%s| expected %s space got %s", test.str, test.pre, actual)
		}
	}
}

func TestJavaTokeniser(t *testing.T) {
	testcases := []testcase{
		{"(", [][]string{{"", "("}}},
		{"int x+=3;", [][]string{{"", "int"}, {" ", "x"}, {"", "+="}, {"", "3"}, {"", ";"}}},
	}
	testTokeniser(t, testcases, "java")
}

func TestLispTokeniser(t *testing.T) {
	testcases := []testcase{
		{"(", [][]string{{"", "("}}},
		{"(define)", [][]string{{"", "("}, {"", "define"}, {"", ")"}}},
		{"(define )", [][]string{{"", "("}, {"", "define"}, {" ", ")"}}},
	}
	testTokeniser(t, testcases, "ss")
}

func TestTextTokeniser(t *testing.T) {
	testcases := []testcase{
		{"abc", [][]string{{"", "abc"}}},
		{"abc. DE3fg", [][]string{{"", "abc"}, {". ", "de"}, {"3", "fg"}}},
    // including special case for Han characters
    {"象形字", [][]string{{"", "象"}, {"", "形"}, {"", "字"}}},
	}
	testTokeniser(t, testcases, "txt")
}

// general test runner
func testTokeniser(t *testing.T, testcases []testcase, filetype string) {
	for _, test := range testcases {
		reader := makeTokenReader(filetype, bufio.NewReader(strings.NewReader(test.str)))
		results := [][]string{}
		for {
			prestring, token, err := reader.ReadToken()
			if err == io.EOF {
				break
			} else if err != nil {
				t.Errorf("Error reading from %s", test.str)
			} else {
				results = append(results, []string{prestring, token})
			}
		}

		if len(results) == len(test.toks) {
			for i, token := range test.toks {
				if token[0] != results[i][0] || token[1] != results[i][1] {
					t.Errorf("Error reading token %d from %s: expected |%s||%s| got |%s||%s|", i, test.str, token[0], token[1], results[i][0], results[i][1])
				}
			}
		} else {
			t.Errorf("Error reading from %s: expected %d tokens, got %d", test.str, len(test.toks), len(results))
		}
	}
}
