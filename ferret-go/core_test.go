/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"testing"
)

func TestGroup(t *testing.T) {
	tests := []struct {
		basedir string
		file1   string
		file2   string
		result  bool
	}{
		{"/", "/file1.txt", "/file2.txt", false},
		{"/", "/a/file1.txt", "/a/file2.txt", true},
		{"/", "/a/file1.txt", "/b/file2.txt", false},
		{"/", "/a/b/file1.txt", "/a/c/file2.txt", true},
		{"/home/", "/home/a/b/file1.txt", "/home/a/c/file2.txt", true},
		{"/home/", "/home/d/b/file1.txt", "/home/a/b/file2.txt", false},
		{"/home/peter/go/src/ferret/data", "/home/peter/go/src/ferret/data/countloc/README.md", "/home/peter/go/src/ferret/data/countloc/README.md", true},
		{"/home/peter/go/src/ferret/data", "/home/peter/go/src/ferret/data/countloc/README.md", "/home/peter/go/src/ferret/data/ferret/core.go", false},
	}

	for _, test := range tests {
		docs := Documents{test.basedir, []File{}, make(map[string][]int), make(map[int]int)}
		if docs.inSameGroup(test.file1, test.file2) != test.result {
			t.Errorf("Same group for %s %s %s was %t not %t", test.basedir, test.file1, test.file2, !test.result, test.result)
		}
	}
}
