/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"github.com/pborman/getopt/v2"
	"os"
  "path/filepath"
  "runtime"
	"sort"
	"strconv"
	"strings"
)

var VERSION = "1.0.1"

// Output similarity results in a CSV format to stdout
func outputResults(docs *Documents, group bool) {
	w := csv.NewWriter(os.Stdout)
	for _, result := range docs.sortedResults(group) {
		output := []string{result.file1, result.file2, strconv.Itoa(result.numcommon), strconv.Itoa(result.numfile1), strconv.Itoa(result.numfile2), fmt.Sprintf("%5.3f", result.similarity)}
		w.Write(output)
	}
	w.Flush()
}

// Output filename - unique counts in a CSV format to stdout
func outputUniqueCounts(docs *Documents, group bool) {
	w := csv.NewWriter(os.Stdout)
	for _, result := range docs.sortedUniqueCounts(group) {
    output := []string{result.filename, strconv.Itoa(result.numunique)}
		w.Write(output)
	}
	w.Flush()
}

// Output list of trigrams in a CSV format to stdout
func outputTrigrams(docs *Documents) {
	type TrigramItem struct {
		trigram string
		count   int
		files   string
	}
	results := []TrigramItem{}

	for trigram, files := range docs.tmap {
		filess := make([]string, len(files))
		for i, id := range files {
			filess[i] = strconv.Itoa(id)
		}
		results = append(results, TrigramItem{trigram, len(files), strings.Join(filess, " ")})
	}

	// sort into decreasing resemblance
	sort.Slice(results, func(i, j int) bool {
		return results[i].count > results[j].count
	})

	w := csv.NewWriter(os.Stdout)
	for _, result := range results {
		w.Write([]string{result.trigram, strconv.Itoa(result.count), result.files})
	}

	w.Flush()
}

func writeXmlComparison(filename1, filename2, outfile string) {
	docs := Documents{"", []File{}, make(map[string][]int), make(map[int]int)}
	docs.files = []File{File{1, filename1, filename1, 0}, File{2, filename2, filename2, 0}}
	docs.readTrigrams()
	docs.computeMatches()

	f, _ := os.Create(outfile)
	defer f.Close()
	w := bufio.NewWriter(f)
	docs.writeXmlHeader(docs.files[0], docs.files[1], w)
	docs.writeXmlDocument(docs.files[0], docs.files[1], w)
	docs.writeXmlDocument(docs.files[1], docs.files[0], w)
	docs.writeXmlTrailer(w)
	w.Flush()
}

func main() {
	var keepgrouped = false
	var showhelp = false
	var outputtrigrams = false
  var outputunique = false
	var showversion = false
	var savexml = false

	// setup options for parameters
	getopt.FlagLong(&keepgrouped, "group", 'g', "Use subdirectory names to group files")
	getopt.FlagLong(&showhelp, "help", 'h', "Show help information")
	getopt.FlagLong(&outputtrigrams, "list-trigrams", 'l', "Output list of trigrams found")
  getopt.FlagLong(&outputunique, "unique-counts", 'u', "Output counts of unique trigrams")
	getopt.FlagLong(&showversion, "version", 'v', "Version number")
	getopt.FlagLong(&savexml, "xml-report", 'x', "filename1 filename2 outfile : Create XML report")
	getopt.SetParameters("filename [filenames...]")

	getopt.Parse()

	if showversion {
		fmt.Println("ferret: version", VERSION)
		os.Exit(0)
	}

	// if help requested or no filename given
	if showhelp {
		getopt.Usage()
		os.Exit(0)
	}

  var inputnames []string

  if runtime.GOOS == "windows" { // on windows, so do own globbing
    for _, name := range (getopt.Args()) {
      if matches, err := filepath.Glob(name); err != nil { // invalid pattern
        inputnames = append(inputnames, name)
      } else if matches != nil { // at least one match
        inputnames = append(inputnames, matches...)
      }
    }
  } else { // on linux or macos
	  inputnames = getopt.Args()
  }

	var docs *Documents

	if savexml {
		if len(inputnames) == 3 {
			writeXmlComparison(inputnames[0], inputnames[1], inputnames[2])
		  os.Exit(0) // we do not want to output anything else, so exit here
		} else {
      fmt.Println("Error: xml option requires three filenames")
			getopt.Usage()
			os.Exit(0)
		}
	} else if len(inputnames) == 1 { // requires single argument to be a directory
		fi, err := os.Lstat(inputnames[0])
		if err != nil || !fi.Mode().IsDir() {
			fmt.Println("Error: single argument source must be a directory")
			getopt.Usage()
			os.Exit(0)
		}
		docs = runFerretFromDir(inputnames[0])
	} else if len(inputnames) > 1 { // requires all strings to be filenames
		for i := range inputnames {
			fi, err := os.Lstat(inputnames[i])
			if err != nil || fi.Mode().IsDir() {
				fmt.Println("Error: multiple argument sources must all be filenames")
				getopt.Usage()
				os.Exit(0)
			}
		}
		docs = runFerretFromFiles(inputnames)
	} else {
    fmt.Println("Error: no filenames provided")
		getopt.Usage()
		os.Exit(0)
	}

	if outputtrigrams {
		outputTrigrams(docs)
  } else if outputunique {
    outputUniqueCounts(docs, keepgrouped)
	} else {
		outputResults(docs, keepgrouped)
	}
}
