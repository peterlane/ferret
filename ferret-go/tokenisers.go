/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"bytes"
	"io"
	"strings"
	"unicode"
)

type codeDefn struct {
	extn    string
	symbols []string
}

var (
	singleCharSymbols = []string{"!", "%", "/", "*", "+", "-", "=", "|", ",",
		"?", ".", "&", "(", ")", "{", "}", "<", ">", ":", ";", "^", "[", "]",
		"\"", "#", "~"}
	actionScriptSymbols = []string{"||=", "&&=", "||", "&&", "===", "!==", ">=",
		"<=", "!=", "==", "/*", "*/", "//", "&=", "|=", "<<=", ">>=", "^=", "%=",
		">>>", ">>>=", "<<", ">>", "+=", "-=", "*=", "/=", "++", "--"}
	cCodeSymbols = []string{"!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
		"*=", "/=", "%=", "&=", "|=", "^=", "::", "->", "//", "<<",
		">>", "##", "/*", "*/", ".*", "->*", "<<=", ">>="}
	cSharpSymbols = []string{"++", "--", "->", "<<", ">>", ">=", "<=", "==", "!=", "||",
		"&&", "+=", "-=", "*=", "/=", "%=", "&=", "|=", "^=", "<<=",
		">>=", "??", "///", "/*", "*/", "//"}
	goCodeSymbols = []string{"+=", "&=", "&&", "==", "!=", "-=", "|=", "||",
		"*=", "^=", "<-", ">=", "<<", "/=", "<<=", "++", ":=", ">>", "%=",
		">>=", "--", "...", "&^", "&^=", "//", "/*", "*/"}
	groovySymbols = []string{"!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
		"*=", "/=", "%=", "&=", "|=", "^=", "//", "<<", ">>", "##",
		"/*", "*/", "/**", "<<=", ">>=", ">>>", ">>>=", "*.@", "<=>", "=~",
		"==~", "*.", ".@", "?:", "?."}
	haskellSymbols = []string{"--", "{-", "-}", "^^", "**", "&&", "||", "<=", "==", "/=",
		">=", "++", "..", "::", "!!", "\\\\", "->", "<-", "=>", ">>",
		">>=", ">@>"}
	javaSymbols = []string{"!=", "++", "--", "==", ">=", "<=", "||", "&&", "+=", "-=",
		"*=", "/=", "%=", "&=", "|=", "^=", "//", "<<", ">>", "/*",
		"*/", "/**", "<<=", ">>=", ">>>", ">>>="}
	luaSymbols = []string{"<=", ">=", "==", "~="}
	phpSymbols = []string{"+=", "-=", "*=", "/=", "%=", ".=", "++", "--", "!=", "==",
		"===", "<>", "!==", ">=", "<=", "||", "&&"}
	prologSymbols = []string{"=<", ">=", "==", "=:=", ":-", "?-"}
	pythonSymbols = []string{"**", "//", ">=", "<=", "==", "!=", "<>", "!=", "+=", "-=",
		"*=", "/=", "%=", "**=", "//=", "<<", ">>"}
	rubySymbols = []string{"**", ">=", "<=", "<<", ">>", "<=>", "=~", "==", "===", "!=",
		"!~", "||", "&&", "..", "...", "+=", "-=", "*=", "/=", "%=",
		"&=", "||=", "&&=", "<<=", ">>=", "**="}
  rustSymbols = []string{"!=", "%=", "&=", "&&", "*=", "+=", "-=", "->", "..", "..=", "...",
     "/=", "<<", "<<=", "<=", "==", "=>", ">=", ">>", ">>=", "^=", "|=",
     "||", "::", "//", "//!", "///", "/*", "*/", "/*!", "/**"}
	vbSymbols = []string{">=", "<=", "<>", "==", "+=", "-=", "*=", "/=", "\\=", "&=",
		"^=", "<<", ">>"}
	xmlSymbols = []string{"<?", "?>", "</", "/>", "<!--", "-->"}

	codedb = []codeDefn{
		{"as", actionScriptSymbols},
		{"actionscript", actionScriptSymbols},
		{"c", cCodeSymbols},
		{"h", cCodeSymbols},
		{"cpp", cCodeSymbols},
		{"cs", cSharpSymbols},
		{"go", goCodeSymbols},
		{"groovy", groovySymbols},
		{"hs", haskellSymbols},
		{"lhs", haskellSymbols},
		{"java", javaSymbols},
		{"lua", luaSymbols},
		{"php", phpSymbols},
		{"pl", prologSymbols},
		{"py", pythonSymbols},
		{"rb", rubySymbols},
    {"rs", rustSymbols},
		{"vb", vbSymbols},
		{"html", xmlSymbols},
		{"xml", xmlSymbols},
	}
	lispExtensions = []string{"clj", "lisp", "lsp", "rkt", "scm", "ss", "sld", "sls", "sps"}
	textExtensions = []string{"adoc", "md", "txt"}
)

func isValidExtension(testExtn string) bool {
	for _, defn := range codedb {
		if testExtn == defn.extn {
			return true
		}
	}
	for _, knownExtn := range append(lispExtensions, textExtensions...) {
		if testExtn == knownExtn {
			return true
		}
	}

	return false
}

type TokenReader interface {
	ReadToken() (string, string, error)
}

type TokenData struct {
	in *bufio.Reader
}

type CodeReader struct {
	TokenData
	symbols map[string]struct{}
}

type LispReader struct {
	TokenData
}

type TextReader struct {
	TokenData
}

// Moves past any whitespace characters, returning string with blanks
func (reader TokenData) SkipBlanks() string {
	blanks := bytes.NewBufferString("")
	for {
		char, _, err := reader.in.ReadRune()
		if err == io.EOF || !unicode.IsSpace(char) {
			reader.in.UnreadRune()
			return blanks.String()
		}
		blanks.WriteRune(char)
	}
}

// Moves past non alphabetic characters, returning string with the nonalphabetic runes
func (reader TokenData) SkipNonAlphabetic() string {
	ignored := bytes.NewBufferString("")
	for {
		char, _, err := reader.in.ReadRune()
		if err == io.EOF || unicode.IsLetter(char) {
			reader.in.UnreadRune()
			return ignored.String()
		}
		ignored.WriteRune(char)
	}
}

// When reading code, there are three broad categories: numbers, symbols and identifiers
// -- numbers are assumed to be made from numbers and . (is code localised?)
// -- symbols are contained in the symbols set, as defined on setup from the file extension
// -- identifiers are the rest
// This produces a useful set of tokens, especially for syntactically correct input code.
// Comments are processed, but also treated as code.
// Function returns the prestring of blanks, the token, and any error
func (reader CodeReader) ReadToken() (string, string, error) {
	// -- define some helper functions
	type notTester func(char rune, _ *bytes.Buffer) bool
	var notIdentifer = func(char rune, _ *bytes.Buffer) bool {
		return !unicode.IsLetter(char) && !unicode.IsNumber(char) && char != '_'
	}
	var notNumber = func(char rune, _ *bytes.Buffer) bool {
		return !unicode.IsNumber(char) && char != '.'
	}
	var notSymbol = func(char rune, result *bytes.Buffer) bool {
		_, ok := reader.symbols[result.String()+string(char)]
		return !ok
	}
	var readToken = func(result *bytes.Buffer, notTest notTester) {
		for {
			char, _, err := reader.in.ReadRune()
			if err == io.EOF || notTest(char, result) {
				break
			} else {
				result.WriteRune(char)
			}
		}
	}

	// -- function body
	prestring := reader.SkipBlanks()
	char, _, err := reader.in.ReadRune()

	if err == io.EOF {
		return prestring, "", io.EOF
	}

	result := bytes.NewBufferString(string(char))

	if unicode.IsNumber(char) || char == '.' { // .......... number
		readToken(result, notNumber)
	} else if _, ok := reader.symbols[string(char)]; ok { // symbol
		readToken(result, notSymbol)
	} else { // ............................................ identifier
		readToken(result, notIdentifer)
	}

	reader.in.UnreadRune()
	return prestring, result.String(), nil
}

func (reader LispReader) ReadToken() (string, string, error) {
	prestring := reader.SkipBlanks()
	char, _, err := reader.in.ReadRune()
	if err == io.EOF {
		return prestring, "", io.EOF
	} else if char == '(' || char == ')' {
		return prestring, string(char), nil
	} else { // must be a symbol - read until EOF, space, ( or )
		result := bytes.NewBufferString(string(char))
		for {
			char, _, err := reader.in.ReadRune()
			if err == io.EOF || unicode.IsSpace(char) || char == '(' || char == ')' {
				reader.in.UnreadRune()
				return prestring, result.String(), nil
			} else {
				result.WriteRune(char)
			}
		}
	}
}

func (reader TextReader) ReadToken() (string, string, error) {
	prestring := reader.SkipNonAlphabetic()
	char, _, err := reader.in.ReadRune()
	if err == io.EOF {
		return prestring, "", io.EOF
	} else { // read next word until EOF, space or !IsLetter
		result := bytes.NewBufferString(string(char))
		for {
			char, _, err := reader.in.ReadRune()
			if err == io.EOF || unicode.IsSpace(char) || !unicode.IsLetter(char) || unicode.In(char, unicode.Han) {
				reader.in.UnreadRune()
				return prestring, strings.ToLower(result.String()), nil
			} else {
				result.WriteRune(char)
			}
		}
	}
}

// Based on the given file extension, create and return an appropriate token reader
func makeTokenReader(extn string, r *bufio.Reader) TokenReader {
	for _, defn := range codedb {
		if extn == defn.extn {
			reader := CodeReader{TokenData{r}, make(map[string]struct{})}
			// fill the symbols map with symbols for the current file's language
			for _, symbol := range append(singleCharSymbols, defn.symbols...) {
				reader.symbols[symbol] = struct{}{}
			}
			return reader
		}
	}

	for _, testExtn := range lispExtensions {
		if extn == testExtn {
			return LispReader{TokenData{r}}
		}
	}

	for _, testExtn := range textExtensions {
		if extn == testExtn {
			return TextReader{TokenData{r}}
		}
	}
	// TODO: Throw error?
	return TextReader{TokenData{}}
}
