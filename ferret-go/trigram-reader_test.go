/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"strings"
	"testing"
)

type trigramcase struct {
	str      string
	trigrams []string
}

func TestJavaTrigramReader(t *testing.T) {
	testcases := []trigramcase{
		{"int x+=3;", []string{"int x +=", "x += 3", "+= 3 ;"}},
	}

	testTrigramReader(t, testcases, "java")
}

func TestLispTrigramReader(t *testing.T) {
	testcases := []trigramcase{
		{"(a)", []string{"( a )"}},
		{"( a )", []string{"( a )"}},
		{"( a b )", []string{"( a b", "a b )"}},
		{"(abc b)", []string{"( abc b", "abc b )"}},
	}

	testTrigramReader(t, testcases, "ss")
}

func TestTextTrigramReader(t *testing.T) {
	testcases := []trigramcase{
		{"", []string{}},
		{"a", []string{}},
		{"a b", []string{}},
		{"a b c", []string{"a b c"}},
		{"some. words, with, punctuation 123 Numbers",
			[]string{"some words with", "words with punctuation", "with punctuation numbers"}},
	}

	testTrigramReader(t, testcases, "txt")
}

// general test runner
func testTrigramReader(t *testing.T, testcases []trigramcase, filetype string) {
	for _, test := range testcases {
		tr := TrigramReader{"", nil, makeTokenReader(filetype, bufio.NewReader(strings.NewReader(test.str))), []string{"", "", ""}, []string{"", "", ""}}
		results := []string{}

		for tr.readTrigram() {
			results = append(results, tr.lastTrigram())
		}

		if len(results) == len(test.trigrams) {
			for i, trigram := range test.trigrams {
				if trigram != results[i] {
					t.Errorf("Error reading trigram %d from %s: expected |%s| got |%s|", i, test.str, trigram, results[i])
				}
			}
		} else {
			t.Errorf("Error reading from %s: expected %d trigrams, got %d", test.str, len(test.trigrams), len(results))
		}
	}
}
