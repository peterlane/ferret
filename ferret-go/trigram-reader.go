/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"os"
	"path/filepath"
	"strings"
)

type TrigramReader struct {
	filename    string
	file        *os.File
	tokenReader TokenReader
	prestrings  []string
	tokens      []string
}

func makeTrigramReader(filename string) TrigramReader {
	r, err := os.Open(filename)
	if err != nil {
		// TODO: report error / terminate?
	}
	return TrigramReader{filename, r,
		makeTokenReader(strings.TrimPrefix(filepath.Ext(filename), "."), bufio.NewReader(r)),
		[]string{"", "", ""}, []string{"", "", ""}}
}

// Read the next trigram
// Returns 'true' if successful
// or 'false' if EOF reached before new trigram could be read
func (reader *TrigramReader) readTrigram() bool {
	for {
		prestring, nextToken, err := reader.tokenReader.ReadToken()
		if err != nil { // failed to read a token, e.g. EOF
			return false
		}
		reader.pushToken(prestring, nextToken)
		if reader.tokens[0] != "" { // needed for first two iterations
			return true
		}
	}
	return true
}

// Return the last read trigram
func (reader *TrigramReader) lastTrigram() string {
	return reader.tokens[0] + " " + reader.tokens[1] + " " + reader.tokens[2]
}

func (reader *TrigramReader) close() {
	reader.file.Close()
}

// Add a new token, keep the length
func (reader *TrigramReader) pushToken(prestring string, token string) {
	reader.prestrings = []string{reader.prestrings[1], reader.prestrings[2], prestring}
	reader.tokens = []string{reader.tokens[1], reader.tokens[2], token}
}
