
desc 'Create a html version of the manual for Fossil'
task :html do
  Dir.chdir('manual') do
    sh 'asciidoctor -n -r asciidoctor-bibtex manual.txt'
    sh 'html2pages --title "Ferret Manual" --part2 "Section" --directory html manual.html'
    rm 'manual.html'
    rm 'html/page-style.css'
    # add fossil header
    Dir.chdir('html') do
      Dir.foreach('.') do |filename|
        next unless filename.end_with?('.html')
        lines = File.new(filename).readlines
        # save with added information
        File.open(filename, 'w') do |file|
          file.puts("<div class='fossil-doc' data-title='Ferret Manual'>")
          lines.each do |line| 
            next if line.start_with?('<html ')
            next if line.include?('page-style.css')
            # -- hack to fix local link split across files
            if filename.include?('Section-1')
              line.gsub!('href="#', 'href="Section-3.html#')
            end
            line.gsub!('Home page','Contents')
            line.gsub!('</html>','')
            file.puts(line) 
          end
          file.puts("</div>")
        end
      end
    end

  end
end

desc 'Create a pdf version of the manual'
task :pdf do
  Dir.chdir('manual') do
    sh 'asciidoctor-pdf -n -r asciidoctor-bibtex manual.txt'
  end
end

desc 'Create a release, with versions for three major architectures'
task :release => [:pdf] do
  # -- create directories
  sh 'mkdir -p linux/ferret'
  sh 'mkdir -p macos/ferret'
  sh 'mkdir -p windows/ferret'
  # -- build executables
  # ---- for linux
  sh 'GOOS=linux; GOARCH=amd64; go build'
  sh 'mv ferret linux/ferret'
  # ---- for macox
  sh 'GOOS=darwin; GOARCH=amd64; go build'
  sh 'mv ferret macos/ferret'
  # ---- for windows
  sh 'GOOS=windows; GOARCH=amd64; go build'
  sh 'mv ferret.exe windows/ferret'
  # -- copy license
  sh 'cp license.txt linux/ferret'
  sh 'cp license.txt macos/ferret'
  sh 'cp license.txt windows/ferret'
  # -- copy stylesheet
  sh 'cp uhferret.xsl linux/ferret'
  sh 'cp uhferret.xsl macos/ferret'
  sh 'cp uhferret.xsl windows/ferret'
  # -- copy manual
  sh 'cp manual/manual.pdf linux/ferret/ferret-manual.pdf'
  sh 'cp manual/manual.pdf macos/ferret/ferret-manual.pdf'
  sh 'cp manual/manual.pdf windows/ferret/ferret-manual.pdf'
  # -- zip each
  Dir.chdir('linux') do
    sh 'zip -r ferret-linux.zip ferret/'
  end
  Dir.chdir('macos') do
    sh 'zip -r ferret-darwin.zip ferret/'
  end
  Dir.chdir('windows') do
    sh 'zip -r ferret-windows.zip ferret/'
  end
  # -- put into downloads folder
  unless Dir.exists?('downloads')
    mkdir 'downloads'
  end
  sh 'cp linux/ferret-linux.zip downloads'
  sh 'cp macos/ferret-darwin.zip downloads'
  sh 'cp windows/ferret-windows.zip downloads'
  # -- tidyup
  rm_rf 'linux'
  rm_rf 'macos'
  rm_rf 'windows'
  rm 'manual/manual.pdf'
end
