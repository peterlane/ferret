/*
    Ferret: Copy detection program
    Copyright (C) 2019, Peter Lane

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
  "bufio"
  "fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type File struct {
	id           int
	pathname     string
	filename     string
	trigramcount int
}

type Documents struct {
	basedir string
	files   []File
	tmap    map[string][]int
	matches map[int]int
}

// adds given filename, if its extension is valid
func (docs *Documents) addFile (filename string) {
		sourcepath, _ := filepath.Abs(filename)
		_, sourcename := filepath.Split(sourcepath)
		ext := strings.TrimPrefix(filepath.Ext(sourcename), ".")
		if isValidExtension(ext) { // keep files we can process
			docs.files = append(docs.files, File{len(docs.files)+1, sourcepath, sourcename, 0})
		}
}

// Locates the names of all files we can process found recursively in given directory
func (docs *Documents) collectFiles(dirname string) {
	collectNames := func(filename string, _ os.FileInfo, _ error) error {
    docs.addFile (filename)
		return nil
	}

	filepath.Walk(dirname, collectNames)
}

// Reads trigrams for each file and builds the tmap
func (docs *Documents) readTrigrams() {
	for i, file := range docs.files {
		reader := makeTrigramReader(file.pathname)
		for reader.readTrigram() {
			docs.addTrigram(&file, reader.lastTrigram())
		}
		reader.close()
		docs.files[i] = file
	}
}

// Constructs information on matching trigrams between documents
func (docs *Documents) computeMatches() {
	for _, filelist := range docs.tmap {
		for _, i := range filelist {
			for _, j := range filelist {
				if i < j {
					key := docs.filesKey(i, j)
					if val, ok := docs.matches[key]; ok {
						docs.matches[key] = val + 1
					} else {
						docs.matches[key] = 1
					}
				}
			}
		}
	}
}

// Checks if given file contains the trigram by looking in the tmap
func (docs *Documents) containsTrigram(file *File, trigram string) bool {
	if fileids, ok := docs.tmap[trigram]; ok {
		// this trigram already has a list of files, so
		// check if this file.id is contained in the list
		idx := sort.SearchInts(fileids, file.id)
		return idx != len(fileids) && fileids[idx] == file.id
	}
	return false
}

// Add give trigram / file to documents
func (docs *Documents) addTrigram(file *File, trigram string) {
	if fileids, ok := docs.tmap[trigram]; ok {
		// this trigram already has a list of files, so
		// check if this file.id is contained in the list
		idx := sort.SearchInts(fileids, file.id)
		if idx == len(fileids) || fileids[idx] != file.id {
			// it is not, so add the new file.id to this trigram's list
			docs.tmap[trigram] = append(docs.tmap[trigram][:idx], append([]int{file.id}, docs.tmap[trigram][idx:]...)...)
			file.trigramcount = file.trigramcount + 1 // found a new trigram for the file
		}
	} else {
		// create a list for this trigram, containing the current file
		docs.tmap[trigram] = []int{file.id}
		file.trigramcount = file.trigramcount + 1
	}
}

// Canonical key format for two file.ids
func (docs *Documents) filesKey(id1 int, id2 int) int {
	if id1 < id2 {
		return id1*len(docs.files) + id2
	} else {
		return id2*len(docs.files) + id1
	}
}

// -- statistical information

// Computes Jaccard correlation of trigrams in the two files
func (docs *Documents) similarity(file1 *File, file2 *File) float64 {
	if nmatches, ok := docs.matches[docs.filesKey(file1.id, file2.id)]; ok {
		total := file1.trigramcount + file2.trigramcount - nmatches
		if total == 0 {
			return 0.0
		} else {
			return float64(nmatches) / float64(total)
		}
	} else {
		return 0.0
	}
}

// Computes proportion of common trigrams with respect to second file
func (docs *Documents) containment(file1 *File, file2 *File) float64 {
	if nmatches, ok := docs.matches[docs.filesKey(file1.id, file2.id)]; ok {
		target := file2.trigramcount
		if target == 0 {
			return 0.0
		} else {
			return float64(nmatches) / float64(target)
		}
	} else {
		return 0.0
	}
}

// -- output arrangements

type Result struct {
	file1      string
	file2      string
	numcommon  int
	numfile1   int
	numfile2   int
	similarity float64
}

type UniqueCount struct {
  filename string
  numunique int
}

func (docs *Documents) removeBaseDir(file string) string {
	return strings.TrimPrefix(strings.TrimPrefix(file, docs.basedir), string(os.PathSeparator))
}

func (docs *Documents) extractGroup (filename string) (string, error) {
  flatfile := docs.removeBaseDir(filename)
  filepath := strings.Split(flatfile, string(os.PathSeparator))
  if len(filepath) > 1 {
    return filepath[0], nil
  } else {
    return filename, fmt.Errorf("Filename %s does not have a group", filename)
  }
}

// assuming file1 and file2 are absolute paths,
// do they start with a directory which is same?
func (docs *Documents) inSameGroup(file1 string, file2 string) bool {
  f1, err1 := docs.extractGroup (file1)
  f2, err2 := docs.extractGroup (file2)

  return err1 == nil && err2 == nil && f1 == f2
}

func (docs *Documents) sortedResults(group bool) []Result {
	results := []Result{}

	// collect all the pairwise results:
	for i, file1 := range docs.files {
		for j, file2 := range docs.files {
			if i < j && (!group || !docs.inSameGroup(file1.pathname, file2.pathname)) {
				results = append(results, Result{
					docs.removeBaseDir(file1.pathname),
					docs.removeBaseDir(file2.pathname),
					docs.matches[docs.filesKey(file1.id, file2.id)],
					file1.trigramcount,
					file2.trigramcount,
					docs.similarity(&file1, &file2)})
			}
		}
	}

	// sort into decreasing resemblance
	sort.Slice(results, func(i, j int) bool {
		return results[i].similarity > results[j].similarity
	})

	return results
}

// Return count of unique trigrams per file, sorted in descending order
// if "group" is true, then use "group" as the name, and total the count for each group
func (docs *Documents) sortedUniqueCounts (group bool) []UniqueCount {
  collect := make(map[string]int) // collect results in a map: name -> count

  for _, fileids := range docs.tmap {
    if len(fileids) == 1 { // found a unique trigram
      file := docs.files[fileids[0]-1] // ASSUMPTION: file idx is id number - 1
      if file.id != fileids[0] {
        fmt.Println ("ERROR: FILE ID IS NOT THE FILE INDEX")
        os.Exit(-1)
      }
      name := file.pathname
      if group { // try to use the group name
        grp, err := docs.extractGroup (file.pathname)
        if err == nil {
          name = grp
        }
      }
      if val, ok := collect[name]; ok {
        collect[name] = val + 1
      } else {
        collect[name] = 1
      }
    }
  }

  results := []UniqueCount{}
  for key, val := range collect {
    results = append(results, UniqueCount{docs.removeBaseDir(key), val})
  }

	// sort into decreasing order of count
	sort.Slice(results, func(i, j int) bool {
		return results[i].numunique > results[j].numunique
	})

  return results
}

func (docs *Documents) writeXmlHeader(file1, file2 File, w *bufio.Writer) {
	w.WriteString("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
	w.WriteString("<?xml-stylesheet type=\"text/xsl\" href=\"uhferret.xsl\" ?>\n")
	w.WriteString("<uhferret>\n")
	w.WriteString(fmt.Sprintf("<common-trigrams>%d</common-trigrams>\n", docs.matches[docs.filesKey(file1.id, file2.id)]))
	w.WriteString(fmt.Sprintf("<similarity>%5.3f</similarity>\n", docs.similarity(&file1, &file2)))
}

func (docs *Documents) writeXmlDocument(file1, file2 File, w *bufio.Writer) {
	// document header
	w.WriteString("<document>\n")
	w.WriteString(fmt.Sprintf("<source>%s</source>\n", file1.pathname))
	w.WriteString(fmt.Sprintf("<num-trigrams>%d</num-trigrams>\n", file1.trigramcount))
	w.WriteString(fmt.Sprintf("<containment>%.3f</containment>\n", docs.containment(&file1, &file2)))
	w.WriteString("<text>\n")
	// document text
	reader := makeTrigramReader(file1.pathname)
	lastWritten := 0     // count of prestring-tokens output
	totalTokens := 2     // keep a count of tokens read/output
	insideBlock := false // a flag to indicate whether inside a 'same' block or not

	for reader.readTrigram() {
		totalTokens += 1

		if docs.containsTrigram(&file2, reader.lastTrigram()) { // writing in block
			if !insideBlock {
				if lastWritten > 0 { // there was a block before, so end it
					w.WriteString("]]></block>")
				}
				w.WriteString("<block text=\"same\"><![CDATA[") // start same block
				insideBlock = true
			}

			if totalTokens-lastWritten > 2 {
				w.WriteString(reader.prestrings[0])
				w.WriteString(reader.tokens[0])
				lastWritten += 1
			}
			if totalTokens-lastWritten > 1 {
				w.WriteString(reader.prestrings[1])
				w.WriteString(reader.tokens[1])
				lastWritten += 1
			}
			if totalTokens-lastWritten > 0 {
				w.WriteString(reader.prestrings[2])
				w.WriteString(reader.tokens[2])
				lastWritten += 1
			}
		} else { // writing out of block
			if lastWritten < totalTokens {
				if insideBlock || lastWritten == 0 { // check if moving from inside same block to unique
					if lastWritten > 0 {
						w.WriteString("]]></block>") // end the last block
					}
					w.WriteString("<block text=\"unique\"><![CDATA[") // start unique block
					insideBlock = false
				}

				if totalTokens-lastWritten > 2 {
					w.WriteString(reader.prestrings[0])
					w.WriteString(reader.tokens[0])
					lastWritten += 1
				}
			}
		}
	}
	// -- tidy up, writing any remaining text
	if totalTokens > 2 && lastWritten < totalTokens {
		if insideBlock {
			w.WriteString("]]></block>") // end the last block
			insideBlock = false
			w.WriteString("<block text=\"unique\"><![CDATA[") // start unique block for remainder
		}
		w.WriteString(reader.prestrings[1])
		w.WriteString(reader.tokens[1])
		w.WriteString(reader.prestrings[2])
		w.WriteString(reader.tokens[2])
	}
	if lastWritten != 0 { // nothing has been written
		w.WriteString("]]></block>")
	}
	// document tail
	w.WriteString("</text></document>\n")
}

func (docs *Documents) writeXmlTrailer(w *bufio.Writer) {
	w.WriteString("</uhferret>\n")
}

// Controls the top-level processing of Ferret,
// returning an instance of Documents from which all results can be extracted
func runFerretFromDir(dirname string) *Documents {
	basedir, _ := filepath.Abs(dirname)
	docs := Documents{basedir, []File{}, make(map[string][]int), make(map[int]int)}

	docs.collectFiles(basedir)
	docs.readTrigrams()
	docs.computeMatches()

	return &docs
}

func runFerretFromFiles(filenames []string) *Documents {
  cwd, _ := os.Getwd ()
	docs := Documents{cwd, []File{}, make(map[string][]int), make(map[int]int)}

  for _, filename := range filenames {
  	docs.addFile(filename)
  }
	docs.readTrigrams()
	docs.computeMatches()

	return &docs
}
